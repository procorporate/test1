USE [pro-corporate_calcs]
GO
/****** Object:  StoredProcedure [dbo].[proc_PayCalcs_u]    Script Date: 02/03/15 15:21:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER       PROCEDURE [dbo].[proc_PayCalcs_u]

 
	@ContractID int = null,		
	@adv int = 0,				--0 for today, 	1 for next working day
	@Holiday int = 2,			--0 for NO holiday Pay Calcs EVEN on wednesday 
								--1 for including holiday pay calcs (on ANY day)
								--2 for no holiday unless a Wednesday (default)

	@express int = 0			--1 = run just express requests

AS
/*


TEST:
exec proc_PayCalcs_u null, 0, 2, 0

select pension_PAE, pension_EER from [pro-corporate].dbo.tblP11_u where paymentdate = dbo.dateonly(getdate())
select * from [pro-corporate].dbo.tblContracts where [contractor id] = 2141702857  
select * from vw_PayCalcs_u_SalesRemit where TR_Sales >0

drop table #TMP1
drop table #TMP_CoSelection
drop table #fees
drop table #vw_PayCalcs_Expenses

delete from TMP_PayCalcs_u_SalesRemit
select *  from [pro-corporate].dbo.tblP11_u where paymentdate = '6 mar 2012' and published is null

select * from [pro-corporate].dbo.tblContracts where [contractor id] = 2141702250    
*/


--Holiday Pay:  declare @Holiday int set @Holiday = 1
if @Holiday = 1 or (  datepart(dw, getdate() ) = 4 and @Holiday = 2)
begin

	set @Holiday = 4

end
--print @holiday


--run Fees:
exec [pro-corporate].dbo.proc_FeesCalculate



--declare @adv int set @adv =1
DECLARE @PaymentDate datetime
set @PaymentDate =
		case	when @adv = 0 then dbo.dateonly(getdate()) 
				else dbo.dateonly([pro-corporate].dbo.fnNextWorkingDay(getdate()))
		end 

--print @PaymentDate


/*
declare @PAYE_Adjust int

select	@PAYE_Adjust = count([contractor id]) 
from	[pro-corporate].dbo.tblContracts c
inner join [pro-corporate].dbo.tblPAYE_Adjustment_Instructions pai on pai.contractorID = c.[contractor id]
where	paytag = 1
--print @PAYE_Adjust
*/

--Populates [hours] field in tblTimesheets from WTD results:
exec proc_PayCalcs_u_Hours


--Populates tblTimesheetPAYEWeek with Timesheet PAYE weeks:
--declare @paymentdate datetime set @paymentdate = '3 jan 2012'
exec proc_PAYE_AllocateWeeksToTS @PaymentDate, null

--Adds any promotional fees / refunds due:
exec proc_PayCalcs_u_Fee_Promotions

--Update Bankowner:
declare @BankOwnerDate as smalldatetime
set @BankOwnerDate = getdate() - 30

exec proc_BankOwner @BankOwnerDate

--drop table #tmp1



--declare @express int set @express = 0 declare @holiday int set @holiday = 4 declare @paymentdate datetime set @paymentdate = '27 feb 2015' declare @adv int set @adv = 0 declare @contractID int set @contractID = null
SELECT	pmd.PayType,
		pmd.RequestType,
		@adv as adv,
		@PaymentDate as paymentDate,
		pmd.[Contractor ID],
		pmd.CompanyID,
		payroll,

		CASE 	WHEN pmd.CalcRequest = 1 then 'BACS'							--HP only Holiday Payments always by BACS
				WHEN b.ContractorID is not null then b.PayMethodRequest			--first option is any outstanding PayRequest (eg. CHAPS, express) - if non, then use default.
				ELSE Pmeth.PayMethodDefault
		END AS PayMethodDefault,

/*
		CASE 	WHEN pmd.requestType = 'HP' then 'BACS'							--Holiday Payments always by BACS
				WHEN b.ContractorID is not null then b.PayMethodRequest			--first option is any outstanding PayRequest (eg. CHAPS, express) - if non, then use default.
				ELSE Pmeth.PayMethodDefault
		END AS PayMethodDefault,
*/

		CalcRequest, 

		coalesce(defs.Pension_Default, 0) AS Pension_Default,		--default pension contributions

		coalesce(defs.CCV_Default, 0) AS CCV_Default,				--default Childcare contributions

		case 	when pmd.PayType = 2 and pmd.payroll <>1 then HPrate 	--use rate invoiced to client if FreePay..
				else
				case 	when HPD_Default is null then dbo.UDF_SH_Factor(dbo.TaxYear(getdate()))
						when HPD_Default < dbo.UDF_SH_Factor(dbo.TaxYear(getdate())) then  dbo.UDF_SH_Factor(dbo.TaxYear(getdate()))	--28 days holiday for 52 weeks * 5 day weeks
						else HPD_Default										--default Holiday Pay DEDUCTIONS (in)
				end
		end AS HPD_Default,						

		--PM changed 28 oct 2011 (Freepay HP out should be same as other non-FP cases):
		--case 	when pmd.PayType = 2 and pmd.payroll <>1 then HPrate	--use rate invoiced to client if FreePay..
				--else
				case 	when HP_Default is null then 28.0/(260-28)
						else HP_Default
				--end
		end AS HP_Default,									--default Holiday Pay PAYMENTS   (out)

		coalesce(HP.HolidayPayRequest, 0) AS HP_Request,		--one-off requests for holiday payment out

		case	when defs.NIC_Default = 0 then 'C' 
				else 'A' 
		end AS NI_Letter

INTO	#TMP1
--declare @express int set @express = 1 declare @holiday int set @holiday = 4 declare @contractID int set @contractID = null select b.PayMethodRequest 
FROM	
	(--declare @contractID int set @contractID = null  declare @holiday int set @holiday = 0
	SELECT DISTINCT
			[Contractor ID],

			co.payroll,

			--case	when HP.contractorID is not null then 'HP'
			--		else 'Remit'
			--end as RequestType,

			case	when HP.contractorID is not null and max(convert(int, c.paytag)) = 1 then 'HP+remit'
					when HP.contractorID is not null and max(convert(int, c.paytag)) =0 then 'HP'
					else 'Remit'
			end as RequestType,

			case	when HP.contractorID is not null and max(convert(int, c.paytag)) = 1 then 2	--mix - both HP and remit
					when HP.contractorID is not null and max(convert(int, c.paytag)) =0 then 1	--HP only
					when HP.contractorID is null and max(convert(int, c.paytag)) = 1 then 0		--Remit only
			end as CalcRequest,

			c.[co id] as CompanyID,

			max(c.CalcType) AS PayType,				--? is standard iBalance; 2 is FreePay

			--NICdefault,						--PM 12 Jul 2010 - field moved to tblPay_Defaults.NIC_default 
			max(coalesce(rm.HPrate, 0)) AS HPrate
--declare @contractID int set @contractID = null  	select  c.[contractor id], hp.contractorid
	FROM 	[pro-corporate].dbo.tblContracts c
	INNER JOIN [pro-corporate].dbo.tblCompanies co on co.[co id] = c.[co id]
	INNER JOIN [pro-corporate].dbo.tblIndividuals i on i.[individual id no] = c.[contractor id]
	LEFT JOIN [pro-corporate].dbo.tblRateMargins rm ON rm.contractid = c.[contract id]
	LEFT JOIN vw_PayCalcs_u_HP_Requests HP ON HP.contractorID = c.[contractor id]
	WHERE	
			(	(@contractID is null and c.paytag = 1) or 
				([contract id] = @ContractID) or
				(@contractID is null and HP.contractorID is not null and @Holiday = 4 )		--HolidayPayments on wednesday only (4)
			)
			
	GROUP BY
			[Contractor ID], c.[co id], HP.contractorID, co.payroll
	)pmd

--select HPD_Default
--default paymethod in bank account details table:
INNER JOIN vw_PayCalcs_u_PayMethod Pmeth on Pmeth.contractorID = pmd.[Contractor ID]

--LEFT JOIN vw_PayCalcs_u_Pension_Default pension_def ON pension_def.ContractorID = pmd.[Contractor ID]
--percentage to deduct for Holiday Pay(deduct and pay):
--LEFT JOIN vw_PayCalcs_u_HP_Default HP_def ON HP_def.ContractorID = pmd.[Contractor ID]

LEFT JOIN [vw_PayCalcs_u_Pay_Defaults] defs ON defs.ContractorID = pmd.[Contractor ID]

--currently set as zero:
--LEFT JOIN vw_PayCalcs_u_HP_Request HPR ON HPR.ContractorID = pmd.[Contractor ID]
--LEFT JOIN vw_PayCalcs_u_HP_Resv HPresv ON HPresv.contractorID = pmd.[contractor id]

--requests for HolidayPay:
LEFT JOIN vw_PayCalcs_u_HP_Requests HP ON HP.contractorID = pmd.[contractor id]

--requests for Express/CHAPS etc:
LEFT JOIN vw_PayCalcs_u_PayInstructions b ON pmd.[Contractor ID] = b.ContractorID

--contractors banned from service:
LEFT JOIN vw_PayCalcs_u_Banned BAN ON BAN.ContractorID = pmd.[Contractor ID]

WHERE	BAN.ContractorID is null and
		(@express = 0 or b.PayMethodRequest = 'express')



--select * from [vw_PayCalcs_u_Pay_Defaults] where contractorid = 2141702857    
--delete from #tmp1 where [contractor ID] <> 2141702917
--select * from #tmp1 order by [contractor id]

UPDATE 	[pro-corporate].dbo.tblPaymentMethods 
SET 	[pro-corporate].dbo.tblPaymentMethods.PaymentDate  = dbo.DateOnly(getdate())
--select #TMP1.*, PayMethodRequest
FROM 	[pro-corporate].dbo.tblPaymentMethods
INNER JOIN #TMP1 ON [pro-corporate].dbo.tblPaymentMethods.ContractorID = #TMP1.[Contractor ID]
WHERE 	[pro-corporate].dbo.tblPaymentMethods.PaymentDate is null and

		(
			(#TMP1.CalcRequest = 0 and PayMethodRequest <>'HOLIDAYPAY')
			or
			(#TMP1.CalcRequest <> 0 and PayMethodRequest = 'HOLIDAYPAY')
		)




--CHAPS Fees:
--Add in any CHAPS/Express fees (for CHAPS, Express or International PaymentMethods):
INSERT INTO [pro-corporate].dbo.tblFees ( [contractor id], [co id], [date], Fee, vat, code, [User], [TimeStamp], notes )
SELECT 	#TMP1.[Contractor ID], 
		CompanyID AS [Co ID],
		dbo.DateOnly(getDate()) AS [date], 
		CASE 	WHEN [payMethodDefault]= 'CHAPS' and CompanyID = -1681424046 THEN 25		--iBalance is £25
				WHEN [payMethodDefault]= 'CHAPS' and CompanyID = 2144302221 THEN 0			--Redego is £0
				WHEN [payMethodDefault] Like 'inter%' THEN 25
				WHEN [payMethodDefault] Like 'exp%' THEN 15
		END AS Fee, 

		CASE 	WHEN [payMethodDefault]= 'CHAPS' and CompanyID = -1681424046 THEN round( 25 * [pro-corporate].dbo.VATRate_v2(0, GETDATE())   ,2)
				WHEN [payMethodDefault]= 'CHAPS' and CompanyID = 2144302221 THEN 0
				WHEN [payMethodDefault] Like 'inter%' THEN round( 25 * [pro-corporate].dbo.VATRate_v2(0, GETDATE())   ,2)
				WHEN [payMethodDefault] Like 'exp%' THEN round( 15 * [pro-corporate].dbo.VATRate_v2(0, GETDATE())   ,2)
		END AS VAT, 

		CASE 	WHEN [payMethodDefault]= 'CHAPS' THEN 10 
				WHEN [payMethodDefault] Like 'inter%' THEN 13 
				WHEN [payMethodDefault] Like 'exp%' THEN 11
		END AS code, 
		'PayCalcs_u_SP' AS [User], 
		getdate() AS [TimeStamp], 
		'appended by proc_PayCalcs_u' AS Notes
FROM 	#TMP1
WHERE	(	
		#TMP1.payMethodDefault = 'chaps' 
		or 
		#TMP1.payMethodDefault Like 'inter%'
		or 
		#TMP1.payMethodDefault Like 'Exp%'
	) 

	and #tmp1.[contractor ID] not in
	(
		SELECT 	[Contractor ID]
		FROM 	[pro-corporate].dbo.tblFees
		WHERE 	deleted = 0 AND 
				[date] = dbo.DateOnly(getDate()) AND 
				Fee >0 AND
				--[user] = 'PayCalcs_u_SP' AND

				(	code =10 Or 	--UK CHAPS
					code =11 Or 	--UK Express
					code =12 Or 	--international CHAPS
					code =13		--international BACS
				)
	)


	and #tmp1.[contractor ID] not in
	(
	select	[individual id no] as [contractor id]
	from	[pro-corporate].dbo.tblIndividuals 
	where	[surname] like 'sample%'
	)



--Find correct Fee:   drop table #fees
select 	#tmp1.[Contractor ID] AS ContractorID,
		feeNet as Fee
into	#fees
from	#tmp1
left join vw_PayCalcs_u_Fees fees on #tmp1.[Contractor ID] = fees.ContractorID 


--select * from #fees

--drop table #vw_PayCalcs_Expenses
select	er.contractorID,
		#tmp1.PaymentDate,
		TrustStatus,
		round(Total, 2) as Total,
		PaidOUT,
		notpaidout,
		Pending,
		disallowed,
		NoExps_ReceiptRequired,
		er.ReadyToUse,
		getdate() as [timestamp]
into	#vw_PayCalcs_Expenses
from	[pro-corporate].dbo.vw_Expenses_Report er
inner join #tmp1 on	#tmp1.[Contractor ID] = er.contractorid 


--select top 1 * from from	[pro-corporate].dbo.vw_Expenses_Report er

delete from TMP_PayCalcs_u_SalesRemit


--Sales Remits (2 versions, one for today, another for advances):
if @adv = 1
begin
	insert into TMP_PayCalcs_u_SalesRemit select * from vw_PayCalcs_u_SalesRemit_ADV
end

if @adv = 0
begin
	insert into TMP_PayCalcs_u_SalesRemit select * from vw_PayCalcs_u_SalesRemit --where contractorid = 2141702816
end


--------------------------------
--select top 10 * from TMP_PayCalcs_u_SalesRemit where contractorid = 2141702716
--delete from TMP_PayCalcs_u_SalesRemit
--select * from  #TMP_CoSelection
--drop table #TMP_CoSelection
CREATE TABLE #TMP_CoSelection (CompanyID int, Payroll int, PayType int, RequestType varchar(12), ContractorID int, PaymentDate datetime, ClearedDate datetime, PeriodBasis int, PAYENo int,
		
		NoPeriods int,			--this is the one returned by query
		NoPeriodsPAYE int,		--this is subject to test to ensure not greater than PAYEweek

		Taxyear varchar(5), TaxCodeNo int, TaxCodeLetter varchar(3), PAYEBasis varchar(10),
		NI_Letter varchar(3), FirstEmployDate datetime, LastEmployDate datetime, Sales money, advance money,
		Pension_EER money, Pension_PAE money,
		CCV money, HPD_Default money, HP_Default money, HP_Request money,
		HPD money, HPC money, HP money, HP_ToDate money, 
		GSC money, GrossSalary money, Additions money,
		PAYE money, EEE money, EER money, GrossSalary_EER money, CreditLimit money, AvFunds money, 

		ExpStatus int, ReadyToUse money, Exp_Unused money, --Exp_Test1 money, 
		Expenses money, 

		Fee money, NMW money, NMW_Basic money, Pension_Resv money, HP_Resv money, SLD money, CSA money, 
		PayToDate money, TaxToDate money, SLD_ToDate money, EEptd money, ERptd money, P45PayToDate money, P45TaxToDate money, 
		PayToDate_Total money, TaxToDate_Total money, LastPAYEno int, PayMethodDefault varchar(50), -- RemitAddressID int,

		--Previous payment in same tax week:
		Sales_Prev money, Expenses_Prev money, Fee_Prev money, 
		NMW_Prev money, Pension_EER_Prev money, CCV_Prev money, GSC_Prev money, 
		GrossSalary_Prev money, PAYE_Prev money, EEE_Prev money, 
		EER_Prev money, NoPeriods_Prev money, Additions_Prev money, HPD_Prev money, HPC_Prev money,
		GrossSalary_EER_Prev money, 
		A14_Prev money, A15_Prev money, A16_Prev money, A17_Prev money, 
		)


INSERT INTO #TMP_CoSelection

SELECT DISTINCT 
		CompanyID,
		payroll,
		payType,
		RequestType,
		cont.[Contractor ID] AS ContractorID, 
		cont.PaymentDate,

		CASE 	WHEN [PayMethodDefault]= 'Chaps' or [PayMethodDefault] = 'FP' or [PayMethodDefault] = 'Express'  THEN cont.PaymentDate
				ELSE dbo.UDF_BACSdueDate(cont.PaymentDate) 
		END AS ClearedDate,

 		52 AS PeriodBasis,
		dbo.UDF_PAYEno(52, cont.PaymentDate) AS PAYEno,

		case	when RequestType = 'HP' then coalesce(np_HP.NoPeriods, 0) 
				when RequestType = 'HP+remit' then coalesce(np_HP.NoPeriods, 0) + coalesce(np.NoPeriods, 0) 
				else coalesce(np.NoPeriods, 0) 
		end AS NoPeriods,

		0 as NoPeriodsPAYE,

		dbo.TaxYear(getdate()) AS TaxYear,

		CASE WHEN TaxCodeNo IS NULL THEN 0 ELSE TaxCodeNo END AS TaxCodeNo,
		CASE WHEN TaxCodeLetter IS NULL THEN 'BR' ELSE TaxCodeLetter END AS TaxCodeLetter,
		CASE WHEN tc.PAYEbasis IS NULL THEN 'week1' ELSE tc.PAYEbasis END AS PAYEbasis,

		NI_Letter AS NI_Letter,
		FirstEmployDate,
		LastEmployDate,

		coalesce(TR_Sales, 0) AS Sales, 
		coalesce(TR_Advance, 0) AS Advance,

		case 	when pension_Default <=1 then pension_Default * coalesce(TR_Sales, 0)
				when pension_Default >1 then pension_Default * (case when coalesce(np.NoPeriods,0) = 0 then 1 else np.NoPeriods end) 
		end AS Pension_EER,

		0 as Pension_PAE,
		--round(coalesce(CCV_Default, 0) + 
		--((coalesce(CCV_Default, 0) * 0.0475) *  dbo.vatrate(0, getdate())),2) AS CCV,		  -- add-in 4.75% service charge + VAT
		CCV_Default as CCV,

		HPD_Default,
		HP_Default,
		HP_Request,
		0 AS HPD,
		0 AS HPC,
		0 AS HP,
		0 AS HP_ToDate,

		0 AS GSC,
		0 AS GrossSalary,
		--coalesce(PAYE_Additions.Additions, 0) AS Additions,
		0 as Additions,
		
		0 AS PAYE,
		0 AS EEE,
		0 AS EER,
		0 AS GrossSalary_EER,

		coalesce(cl.CreditLimit, 0) AS CreditLimit,
		-9999.00 AS AvFunds,

		TrustStatus AS ExpStatus,				

		coalesce(ReadyToUse, 0) AS ReadyToUse,  					--PM 31 Oct 2008 - GROSS expenses paid out - not separate net and vat

		0 AS Exps_unused,

		0 AS Expenses,


		coalesce(#fees.fee, 0) AS Fee,

		CASE 	WHEN NMW < 0 THEN 0
				WHEN NMW > coalesce(TR_Sales, 0) - coalesce(#fees.fee, 0) THEN coalesce(TR_Sales, 0) - coalesce(#fees.fee, 0)
				ELSE coalesce(NMW, 0)
		END AS NMW,

		coalesce(NMW_Basic, 0) AS NMW_Basic,

		--Previous Gross Reserves (excluding today's):
		coalesce(Pension_Resv, 0) AS Pension_Resv,
		coalesce(HP_Resv, 0) AS HP_Resv,
		coalesce(tc.SLD, 0) as SLD, 
		0 AS CSA ,
		PayToDate, TaxToDate, SLD_ToDate, EEptd, ERptd, P45PayToDate, P45TaxToDate, 
		coalesce(PayToDate_Total, 0) as PayToDate_Total,
		coalesce(TaxToDate_Total, 0) as TaxToDate_Total, 
		LastPAYEno,


		cont.PayMethodDefault, 
		--cont.RemitAddressID,

		--Previous payment in same tax week:
		--Sales_Prev, Expenses_Prev, Fee_Prev, NMW_Prev, Pension_EER_Prev, GSC_Prev, PAYE_Prev, EEE_Prev, EER_Prev
		--0 a, 0 b, 0 c, 0 d, 0 e, 0 f, 0 g, 0 h, 0 i 
		coalesce(Sales_Prev, 0) Sales_Prev, coalesce(Expenses_Prev, 0) Expenses_Prev, coalesce(Fee_Prev, 0) Fee_Prev, 
		coalesce(NMW_Prev, 0) NMW_Prev, coalesce(Pension_EER_Prev, 0) Pension_EER_Prev, coalesce(CCV_Prev,0) CCV_Default_Prev, 
		coalesce(GSC_Prev, 0) GSC_Prev, 
		coalesce(GrossSalary_Prev, 0) GrossSalary_Prev, coalesce(PAYE_Prev, 0) PAYE_Prev, coalesce(EEE_Prev, 0) EEE_Prev,
		coalesce(EER_Prev, 0) EER_Prev, coalesce(NoPeriods_Prev, 0) NoPeriods_Prev, coalesce(Additions_Prev, 0) Additions_Prev,
		coalesce(HPD_Prev, 0) HPD_Prev, coalesce(HPC_Prev, 0) HPC_Prev, coalesce(GrossSalary_EER_Prev, 0) GrossSalary_EER_Prev,

		coalesce(A14_Prev, 0) A14_Prev, coalesce(A15_Prev, 0) A15_Prev, coalesce(A16_Prev, 0) A16_Prev, coalesce(A17_Prev, 0) A17_Prev

--select cont.*
FROM	#TMP1 cont
LEFT JOIN vw_PayCalcs_u_TaxCodes tc ON cont.[contractor id] = tc.ContractorID and cont.CompanyID = tc.[co id]
LEFT JOIN #fees ON cont.[contractor id] = #fees.ContractorID 
LEFT JOIN vw_CreditLimits cl ON cont.[contractor id] = cl.contractorID 
LEFT JOIN #vw_PayCalcs_Expenses expenses ON cont.[contractor id] = expenses.ContractorID 

LEFT JOIN TMP_PayCalcs_u_SalesRemit sr on cont.[contractor id] = sr.ContractorID
LEFT JOIN vw_Paycalcs_u_NoPeriods np on np.contractorID = cont.[contractor id]
LEFT JOIN vw_PayCalcs_u_NoPeriods_HP np_HP on np_HP.contractorID = cont.[contractor id]

LEFT JOIN vw_PayCalcs_u_Dates dts ON cont.[contractor id] = dts.ContractorID and dts.coid = cont.companyID
LEFT JOIN vw_PayCalcs_u_PayToDate ptd ON ptd.ContractorID = cont.[contractor id] and ptd.CoID = cont.companyID
LEFT JOIN vw_PayCalcs_u_NMW nmw ON cont.[contractor id] = nmw.ContractorID 

LEFT JOIN
	(--Previous payment in same tax week:
	select	p.ContractorID AS CiD, 
			--required to calc. GSC:
			sum(p.Sales) AS Sales_Prev,
			sum(p.Expenses) AS Expenses_Prev,
			sum(p.Fee) AS Fee_Prev,
			sum(p.NMW) AS NMW_Prev,
			sum(p.Pension_EER) AS Pension_EER_Prev,
			sum(p.CCV) AS CCV_Prev,
			Sum(p.GSC) AS GSC_Prev,
			Sum(p.GrossSalary) AS GrossSalary_Prev,
			sum(p.GrossSalary_EER) AS GrossSalary_EER_Prev,
			Sum(p.PAYE) AS PAYE_Prev,
			Sum(p.EEE) AS EEE_Prev,
			Sum(p.EER) AS EER_Prev,
			Sum(p.NoPeriods) AS NoPeriods_Prev,
			Sum(p.Additions) AS Additions_Prev,
			Sum(p.HPC) AS HPC_Prev,
			Sum(p.HPD) AS HPD_Prev

			--
			,
			sum(p.[A14]) as A14_Prev,		-- coalesce([1a], 0),
			sum(p.[A15]) as A15_Prev,		-- coalesce([1b], 0), 
			sum(p.[A16]) as A16_Prev,		-- coalesce([1c], 0),
			sum(p.[A17]) as A17_Prev		-- coalesce([1d], 0) 


			--
	from	[pro-corporate].dbo.tblP11_u p
	inner join #TMP1 c on c.[contractor ID] = p.contractorid
	where	p.taxyear = dbo.taxyear(getdate()) and		-- must be this taxyear
			p.PAYEno = dbo.UDF_PAYEno(52, getdate()) and	-- must be the same tax week
			p.paymentdate <> dbo.DateOnly(getdate()) and	-- exclude today's payments
			p.published is not null						-- must be published payment
	group by
			p.ContractorID, p.PAYEno, p.TaxYear
	)prev
	on prev.CiD = cont.[Contractor ID]


--select contractorID, [a14_prev], [a15_prev], [a16_prev], [a17_prev] from #TMP_CoSelection 

--select * from vw_P11_Fields where contractorid = 2141702250 and paymentdate = '25 apr2013'

--select RemitAddressID from [pro-corporate].dbo.tblP11_u

--select * from #TMP_CoSelection where contractorid <>2141701787


--use this for PAYE / NIC calcs (just in case table NoPeriods is zero)



------------------------
--0==.Childcare vouchers
UPDATE	#TMP_CoSelection
SET		CCV = 
--select CCV, NoPeriods, [pro-corporate].dbo.fncontractorname(contractorid, 2),
		--CCV is CCV weekly default + admin + vat x NoPeriods
		round(	(	coalesce(CCV, 0) + 
					(coalesce(CCV, 0) * 0.0475 * (1+dbo.vatrate(0, getdate() ) ))
				)
				*  NoPeriods
		
		,2)
FROM	#TMP_CoSelection
WHERE	CCV <>0


--0. Check Expenses limits:
UPDATE	#TMP_CoSelection
SET		Expenses = exps.Expenses
		--ExpensesVAT = exps.ExpensesVAT
--select exps.ContractorID, Sales, Fee, NMW, Exp_test1, ReadyToUse, exps.expenses
FROM
	(
	SELECT	ContractorID,

		--stop negative expenses unless TrustStatus is 3(claw-back) | TrustStatus 2 is 'no expenses', TrustStatus 1 is 'receipts required'
		CASE 	WHEN ExpStatus = 3 THEN (case when ReadyToUse > 0 then 0 else ReadyToUse end )	--claw-back or £0
				WHEN ExpStatus = 2 THEN 0								--always £0
				ELSE
				CASE 	WHEN ReadyToUse < 0 THEN 0				--Limited to positive figures
						WHEN ReadyToUse > Exp_Test THEN Exp_Test 
						ELSE ReadyToUse							--Limit to Sales less Fees less NMW
				END
		END AS Expenses

		--stop paying ExpVAT unless TrustStatus is 3(claw-back) | TrustStatus 2 is 'no expenses', TrustStatus 1 is 'receipts required'
		--CASE 	WHEN ExpStatus <>1 THEN (case when ExpensesVAT > 0 then 0 else ExpensesVAT end )	--claw-back or £0							--always £0
		--		ELSE 0	--ExpensesVAT
		--END AS ExpensesVAT
	
	FROM 	
		(-- Expenses limit:  greater of £0 or (Sales - NMW - fee)
		select	ContractorID,
				Sales, NMW, NMW_Prev, fee, ReadyToUse, ExpStatus, Expenses_Prev,-- ExpensesVAT,

				--Total Exp_Text:
				case 	when (Sales+Sales_Prev) - (NMW+NMW_Prev) - (fee+fee_Prev) - (CCV+CCV_Prev) - (Pension_EER + Pension_EER_Prev) <0 then 0
						else (Sales+Sales_Prev) - (NMW+NMW_Prev) - (fee+fee_Prev) - (CCV+CCV_Prev) - (Pension_EER + Pension_EER_Prev) 
				end -Expenses_Prev AS Exp_Test

		from	#TMP_CoSelection
		--where 	contractorID = 2141699760
		)b
	)exps

INNER JOIN #TMP_CoSelection ON #TMP_CoSelection.ContractorID = exps.ContractorID

--select (Sales+Sales_Prev), (NMW+NMW_Prev), (fee+fee_Prev), (CCV+CCV_Prev), (Pension_EER + Pension_EER_Prev)  from #TMP_CoSelection 


--1.  Check pension request is covered by Sales:
UPDATE	#TMP_CoSelection
--select Sales, Fee, NMW, Pension_EER, GSC from #TMP_CoSelection
SET	Pension_EER = 	case	when RequestType = 'HP' then 0 else
							case 	when Pension_EER > Sales - Fee - NMW then Sales - Fee - NMW 
									else Pension_EER
							end
					end

--1a.  Check Childcare voucher request is covered by Sales:
UPDATE	#TMP_CoSelection
--select Sales, Fee, NMW, Pension_EER, CCV_Default from #TMP_CoSelection
SET	CCV = 	case 	when CCV = 0 then 0
					when CCV > Sales - Fee - NMW - Pension_EER then Sales - Fee - NMW -Pension_EER
					else CCV
					end

--select paytype from #TMP_CoSelection


--2.  Set Basic GSC:
UPDATE	#TMP_CoSelection
SET	GSC = gsc.GSC 
--select gsc.*
FROM
	(--TOTAL Gross Salary Cost:
	SELECT	ContractorID, GSC_prev, HPC_prev, HPD_prev, 

	--sales, expenses, fee, nmw, ccv,
			dbo.UDF_GSC_u(Sales_Prev, Expenses_Prev, Fee_Prev, NMW_Prev, Pension_EER_Prev+CCV_Prev, (HPD_Prev)-(HPC_Prev), 1) as GSCprev,
			dbo.UDF_GSC_u(Sales, Expenses, Fee, NMW, Pension_EER+CCV, (HPD)-(HPC), 1) as GSCtr,
			dbo.UDF_GSC_u(Sales+Sales_Prev, Expenses+Expenses_Prev, Fee+Fee_Prev, NMW+NMW_Prev, Pension_EER+Pension_EER_Prev+CCV+CCV_Prev, (HPD+HPD_Prev)-(HPC+HPC_Prev), 1) as GSCtotal,
--(HPD+HPD_Prev)-(HPC+HPC_Prev) as HPtotal,
--HPD, HPD_Prev, HPC,HPC_Prev,
--Sales+Sales_Prev as Sales, Expenses+Expenses_Prev as Exps, Fee+Fee_Prev as Fee, NMW+NMW_Prev as NMW, Pension_EER+Pension_EER_Prev+CCV+CCV_Prev as P_CCV, (HPD+HPD_Prev)-(HPC+HPC_Prev) as HP,
			case 	when PayType = 2 then 0
					else dbo.UDF_GSC_u(Sales+Sales_Prev, Expenses+Expenses_Prev, Fee+Fee_Prev, NMW+NMW_Prev, Pension_EER+Pension_EER_Prev+CCV+CCV_Prev, (HPD+HPD_Prev)-(HPC+HPC_Prev), 1)   --'1' is the switch for using ir35 5% allowance or not.
					 - GSC_Prev 
			end AS GSC	

	FROM 	#TMP_CoSelection
	)gsc

INNER JOIN #TMP_CoSelection ON #TMP_CoSelection.ContractorID = gsc.ContractorID

--where gsc.contractorid = 2141703631

--PAE START +++++++++++++++++++++++++++++++++++++++++++++

--2a.  Set Pension Auto-Enrolment:
UPDATE	#TMP_CoSelection
SET		Pension_EER = #TMP_CoSelection.Pension_EER + pen.Pension_PAE,		--add together, so both contributions are collected
		Pension_PAE = pen.Pension_PAE
--select pen.Pension_PAE, #TMP_CoSelection.Pension_EER, #TMP_CoSelection.GSC, NoPeriods
FROM
		(
		SELECT	ContractorID, Pension_EER, GSC,

				ROUND(
						--0. Does GSC trigger a PAE contribution:?
				case	when GSC < PAE_Trigger_GSC * [NoPeriods]  then 0
						
						--1. Here the tmpGSC is below all threshholds NicER and PAE:
						--when GSC <= [PAE_LEL]*[NoPeriods] THEN  0


						--2. Here the tmpGSC is above the PAE and below the NicER threshholds  (at the moment this gap is £39 per week, 2% of which is 78p per week):
						--when GSC > [PAE_LEL]*[NoPeriods] AND GSC <= ( [EerET]+ ([EerET]-[PAE_LEL]) *[PAE_Contribution] ) *[NoPeriods] THEN
						--
						--	( GSC -[PAE_LEL]*[NoPeriods]) * [PAE_Contribution]


						--3. Here the tmpGSC is above the grossed up Trigger and below or equal to the grossed up  [PAE_UEL] (ie >= PAE_GSCmax):
						when GSC <= PAE_GSCmax * NoPeriods THEN 
								case	when GSC > NMW then
										( GSC - ([EerET]-[PAE_LEL])*[PAE_Contribution]*[NoPeriods]   -  [EerET]*[NoPeriods] ) / ( 1+[NiEerRate]+[PAE_Contribution])*[PAE_Contribution] + ([EerET]-[PAE_LEL])*[PAE_Contribution]*[NoPeriods]
										else
										(GSC -  [EerET]*[NoPeriods]) /  ( 1+[NiEerRate])*[PAE_Contribution] + ([EerET]-[PAE_LEL])*[PAE_Contribution]*[NoPeriods]
								end

						--4. Finally the tmpGSC is above the Pension_GSCmax:

						else Pension_EERmax * NoPeriods

				end , 2) as Pension_PAE

				--PAE_Trigger_GSC, PAE_GSCmax, Pension_EERmax, NoPeriods, 
				--PAE_LEL, PAE_UEL, PAE_Contribution,[pro-corporate].dbo.UDF_PAE_Required(contractorID) as PAE_Required,
		FROM	#TMP_CoSelection b
		inner join vw_PayCalcs_u_Pension_Data pd on pd.taxyear = b.taxyear
		WHERE	[pro-corporate].dbo.UDF_PAE_Required(contractorID, 0) = 1			--only where required
--and  contractorID = 2141703631

		)pen
INNER JOIN #TMP_CoSelection ON #TMP_CoSelection.ContractorID = pen.ContractorID

--select * from vw_PayCalcs_u_Pension_Data where taxyear = '13/14'





--2aa. refund where opted out after previous deductions:
UPDATE	#TMP_CoSelection
SET		Pension_EER = #TMP_CoSelection.Pension_EER + refund.Pension_PAE,		--add together, so both contributions are collected
		Pension_PAE = refund.Pension_PAE
--select  #TMP_CoSelection.Pension_EER, #TMP_CoSelection.GSC, NoPeriods
FROM
		(
		select	p.contractorID, 
				--[pro-corporate].dbo.udf_employID(p.contractorID) as EmployID,
				count(*) as NoContributions, 
				-sum(Pension_PAE) as Pension_PAE				--refund
		from	[pro-corporate].dbo.tblP11_u p
		inner join 
				(
				select	--dbo.fnContractorName(contractorid, 5) as c,  
						contractorid, employID, EmployDate, FinishDate,
						PAEenroll, PAEoptOut, PAEnotified
				from	[pro-corporate].dbo.tblEmploy

				where	--conditions for re-paying PAE:
						[pro-corporate].dbo.UDF_PAE_Required(contractorID , 1) = 1		--parameter 1 tests for re-payment.
						--PAEenroll < getdate() and			--already enrolled
						--PAEoptOut < getdate()				--have requested opt-out

				)a
				on a.employID = [pro-corporate].dbo.udf_employID(p.contractorID)

		------------
		where	Pension_PAE <>0 and			--previous deductions
				published is not null and

				--deductions in current employment
			 	paymentdate between EmployDate and coalesce(FinishDate, getdate()+1 )
		group by 
				p.contractorid
		)refund

INNER JOIN #TMP_CoSelection ON #TMP_CoSelection.ContractorID = refund.ContractorID



--2b.  Run GSC again, as it will be reduced by Pension PAE above...
UPDATE	#TMP_CoSelection
SET	GSC = gsc.GSC 
--select gsc.*
FROM
	(--TOTAL Gross Salary Cost:
	SELECT	ContractorID, GSC_prev, HPC_prev, HPD_prev,

			dbo.UDF_GSC_u(Sales_Prev, Expenses_Prev, Fee_Prev, NMW_Prev, Pension_EER_Prev+CCV_Prev, (HPD_Prev)-(HPC_Prev), 1) as GSCprev,
			dbo.UDF_GSC_u(Sales, Expenses, Fee, NMW, Pension_EER+CCV, (HPD)-(HPC), 1) as GSCtr,
			dbo.UDF_GSC_u(Sales+Sales_Prev, Expenses+Expenses_Prev, Fee+Fee_Prev, NMW+NMW_Prev, Pension_EER+Pension_EER_Prev+CCV+CCV_Prev, (HPD+HPD_Prev)-(HPC+HPC_Prev), 1) as GSCtotal,
--(HPD+HPD_Prev)-(HPC+HPC_Prev) as HPtotal,
--HPD, HPD_Prev, HPC,HPC_Prev,
--Sales+Sales_Prev as Sales, Expenses+Expenses_Prev as Exps, Fee+Fee_Prev as Fee, NMW+NMW_Prev as NMW, Pension_EER+Pension_EER_Prev+CCV+CCV_Prev as P_CCV, (HPD+HPD_Prev)-(HPC+HPC_Prev) as HP,
			case 	when PayType = 2 then 0
					else dbo.UDF_GSC_u(Sales+Sales_Prev, Expenses+Expenses_Prev, Fee+Fee_Prev, NMW+NMW_Prev, Pension_EER+Pension_EER_Prev+CCV+CCV_Prev, (HPD+HPD_Prev)-(HPC+HPC_Prev), 1)   --'1' is the switch for using ir35 5% allowance or not.
					 - GSC_Prev 
			end AS GSC	

	FROM 	#TMP_CoSelection
	)gsc

INNER JOIN #TMP_CoSelection ON #TMP_CoSelection.ContractorID = gsc.ContractorID


--2c. check that PAE doesn't exceed total:
UPDATE	#TMP_CoSelection
SET		Expenses = #TMP_CoSelection.Expenses - pen.PenAdj
--select pen.contractorID, PenAdj, GSC, Fee, pen.Expenses
FROM

	(
	SELECT	a.ContractorID, Expenses,
			case	when Expenses >= PenAdj then PenAdj
					else Expenses					--check we don't try and set negative expenses
			end as PenAdj
	FROM
			(--Amount Pension is over the total
			SELECT	ContractorID, Expenses,
					--Noperiods, GSC, GrossSalary, Pension_PAE,
					--GSC, Fee, Expenses, Sales, Sales_Prev, Pension_EER, Pension_PAE, Pension_EER_Prev,
					--Sales+Sales_Prev as S,
					--Sales+Sales_Prev - (Fee+Fee_Prev + CCV+CCV_Prev + Expenses+Expenses_Prev + GSC+GSC_Prev) as PensionCompare,
					--Pension_EER+Pension_EER_Prev - ( Sales+Sales_Prev - (Fee+Fee_Prev + CCV+CCV_Prev + Expenses+Expenses_Prev + GSC+GSC_Prev))  as R,
					case	when Sales+Sales_Prev - (Fee+Fee_Prev + CCV+CCV_Prev + Expenses+Expenses_Prev + GSC+GSC_Prev) < Pension_EER+Pension_EER_Prev then 
								 Pension_EER+Pension_EER_Prev - (Sales+Sales_Prev - (Fee+Fee_Prev + CCV+CCV_Prev + Expenses+Expenses_Prev + GSC+GSC_Prev))
							else 0
					end as PenAdj
			FROM 	#TMP_CoSelection
			WHERE	Pension_PAE >0 
			)a
	WHERE	PenAdj <>0
	)pen
INNER JOIN #TMP_CoSelection ON #TMP_CoSelection.ContractorID = pen.ContractorID



--PAE end +++++++++++++++++++++++++++++++++++++++++++++



--3. Set HP Deductions:
UPDATE	#TMP_CoSelection
SET	HPD = 	case 	--when PayType = 2 then round(dbo.UDF_SH_Factor(dbo.taxyear(getdate())) * Sales, 2)
					when PayType = 2 then round(HPD_Default * sales, 2)
					else round(NMW_Basic * HPD_Default, 2)
			end
--select   GSC * HPD_Default AS HPD, GSC, HPD_Default, HP_Default, round(HPD_Default * sales, 2), HPD
FROM	#TMP_CoSelection

--select HPD_Default, NMW_Basic, HPD, (Sales - Expenses + Additions+Additions_Prev - GrossSalary_Prev) from #TMP_CoSelection

--4. Set HP costs:
UPDATE	#TMP_CoSelection
SET	HPC = 	case 	--when PayType = 2 and payroll <> 1 then 0
					when PayType = 2 and requestType <> 'HP' then round(HP_Default * sales, 2) 
					else round((NMW_Basic * HP_Default) + HP_Request, 2)
			end
--select NMW_Basic, sales, paytype, payroll, HPC, HPD, requestType, NMW_Basic, HP_Default, HP_Request
FROM	#TMP_CoSelection



--5:  Update GrossSalary (EXCLUDING HolidayPay):
UPDATE	#TMP_CoSelection
SET		GrossSalary = gs.GrossSalary
FROM	(
		select	ContractorID,
				case	when RequestType = 'HP' then HP_Request	
						else
						case 	--when PayType = 2 then Sales - Expenses + Additions+Additions_Prev - GrossSalary_Prev		--FreePay  PM commented out 23 Jul 2010
								when PayType = 2 then Sales - Expenses + HPC	-- + Additions+Additions_Prev - GrossSalary_Prev 	--FreePay
								else
								dbo.UDF_GSest_u(GSC+GSC_Prev, TaxYear, periodBasis, NoPeriods+NoPeriods_Prev) + Additions+Additions_Prev	--Normal Remit
									- GrossSalary_Prev 
						end
				end AS GrossSalary
		FROM 	#TMP_CoSelection
		)gs

INNER JOIN #TMP_CoSelection ON #TMP_CoSelection.ContractorID = gs.ContractorID

--select sales, grossSalary, gsc, paytype, HP_Request, gsc_prev, requestType, NoPeriods, NoPeriods_Prev from #TMP_CoSelection


--5A:  Update EER:
UPDATE	#TMP_CoSelection
SET		EER =	case 	when PayType = 2 then 			--Freepay
							dbo.UDF_EERemployee(GrossSalary+GrossSalary_Prev, 0, 0, NoPeriods+NoPeriods_Prev, PeriodBasis, TaxYear )
						else	case	when RequestType = 'HP' then 0 
										else GSC-GrossSalary 
								end
				end

--select paytype, requesttype from #TMP_CoSelection


--6a. Set HP_Resv (gross HP reserves):
UPDATE	#TMP_CoSelection
--select HP_Resv + (HPD - HPC) as calc, HP_Resv, HPD, HPC from #TMP_CoSelection

SET		HP_Resv = 	case	when RequestType = 'HP' then HP_Resv - GrossSalary
							else HP_Resv + (HPD - HPC) 		--previous carried fwd + today's
					end
FROM	#TMP_CoSelection


-- ++++++++++++++++++++++++++++++++ HOLIDAY PAY ENDS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++


--select HPD, HPC, gsc, grossSalary from #TMP_CoSelection where contractorid = 2141702665


--6c. Set GrossSalary (adjust for Holiday Pay):	
UPDATE	#TMP_CoSelection
SET		GrossSalary =	case	when PayType = 2 then GrossSalary				--Freepay	
								when RequestType = 'HP' then GrossSalary		--Holiday Pay			
								else GrossSalary - (HPD - HPC)						--other umbrella brands
						end

--select GrossSalary, (HPD-HPC) from #TMP_CoSelection
--select contractorID, GSC, GrossSalary, EER, (HPD - HPC) as HP, PAYE, EEE from #TMP_CoSelection


--7a. Check Number of periods doesn't exceed PAYE week number:
UPDATE	#TMP_CoSelection
SET		NoPeriodsPAYE = case	when NoPeriods+NoPeriods_Prev = 0 then 1		--never do zero number of periods	
								--when NoPeriods+NoPeriods_Prev > PAYENo then PAYEno  -- JR temp Removed 8 April 2011
								else NoPeriods+NoPeriods_Prev		
						end
FROM	#TMP_CoSelection

--select NoPeriods+NoPeriods_Prev from #TMP_CoSelection 


/***** JR debug*/
--drop table ##jr1
--select GrossSalary+GrossSalary_Prev as pay, NoPeriodsPAYE+NoPeriods_Prev as periods, PeriodBasis, TaxYear
--into ##jr1
--from #TMP_CoSelection
/***** JR debug */



--7. Set PAYE/NIC:	
UPDATE	#TMP_CoSelection
--select HP, HPC, GrossSalary, GSC from #TMP_CoSelection
SET		PAYE = 	paye.PAYE,
		EEE = paye.EEE  
--select paye.*
FROM	
	(
	select	ContractorID,

--NI_Letter, EEE_prev,GrossSalary_Prev, GrossSalary,

		--Total PAYE:
		CASE 	WHEN PAYEbasis = 'week1' or PAYEno = 53 THEN 

					dbo.UDF_PAYE(GrossSalary+GrossSalary_Prev, TaxCodeNo, TaxCodeLetter, PeriodBasis, NoPeriodsPAYE, 0, 0, 1, TaxYear) - PAYE_Prev 

				ELSE
					dbo.UDF_PAYE(GrossSalary, TaxCodeNo, TaxCodeLetter, PeriodBasis, PAYENo, PayToDate_Total, TaxToDate_Total, 0, TaxYear)

		END AS PAYE,


		--Total EEE:
		case when NI_Letter = 'C' then 0 else 
				dbo.UDF_EEE(GrossSalary+GrossSalary_Prev, 0, 0, NoPeriods+NoPeriods_Prev, PeriodBasis, TaxYear, null)
				- EEE_Prev 
		end AS EEE

	from 	#TMP_CoSelection
	)paye

INNER JOIN #TMP_CoSelection ON #TMP_CoSelection.ContractorID = paye.ContractorID



--8. Set GSC again:
UPDATE	#TMP_CoSelection
SET		GSC = 	GrossSalary + EER + HPD - HPC
--select contractorid, GSC, GrossSalary, EER, GrossSalary + EER + HPD - HPC
FROM	#TMP_CoSelection


--select contractorid, gsc from #TMP_CoSelection



--9.  Update CSA:
UPDATE	#TMP_CoSelection
SET		CSA = b.CSA
--select b.*
FROM
		(
			select	c.ContractorID,
					--a.adjustment_Type, 
					--GrossSalary,EEE,PAYE,
					coalesce(dbo.UDF_PAYE_CSA(GrossSalary+GrossSalary_Prev-(EEE+EEE_Prev)-(PAYE+PAYE_Prev), csa.protected, csa.csa, c.NoPeriodsPAYE, csa.BFW ),0) AS CSA
			from	#TMP_CoSelection c
			left join vw_PayCalcs_u_CSA csa on csa.contractorID = c.contractorID
		)b
INNER JOIN #TMP_CoSelection ON #TMP_CoSelection.ContractorID = b.ContractorID


--10.  Update GrossSalary_EER:  (actual EER on GrossSalary)  -- different from EER as this includes EER on Holiday Pay
UPDATE	#TMP_CoSelection
SET		GrossSalary_EER = dbo.UDF_EERemployee(GrossSalary+GrossSalary_Prev, 0, 0, NoPeriods+NoPeriods_Prev, PeriodBasis, TaxYear ) - GrossSalary_EER_Prev
						  
--select dbo.UDF_EERemployee(GrossSalary+GrossSalary_Prev, 0, 0, NoPeriods+NoPeriods_Prev, PeriodBasis, TaxYear), GrossSalary, NoPeriods, PeriodBasis, TaxYear
FROM	#TMP_CoSelection

--select PAYE from #TMP_CoSelection


--11.  Update SLD:
UPDATE	#TMP_CoSelection
SET		SLD = coalesce(dbo.UDF_PAYE_SLD(GrossSalary, NoPeriodsPAYE, PeriodBasis, Taxyear),0)
--select sld, coalesce(dbo.UDF_PAYE_SLD(GrossSalary, NoPeriodsPAYE, PeriodBasis, Taxyear),0) AS SLDc
from	#TMP_CoSelection 
where	sld <> 0

--select PAYE from #TMP_CoSelection


--12.  Update Fee (+ others) for Holiday Pay calcs:
UPDATE	#TMP_CoSelection
SET		Fee = 0,
		--EER = GrossSalary_EER,
		gsc = GrossSalary + EER
--select contractorID, fee, EER, GSC, GrossSalary_EER, GrossSalary + EER
FROM	#TMP_CoSelection 
WHERE	RequestType = 'HP'


--select PAYE from #TMP_CoSelection



--select NoPeriodsPAYE, fee, expenses, ccv, gsc, GrossSalary, EER, EEE, Pension_EER, Pension_PAE, PAYE, NoPeriods from #TMP_CoSelection where contractorid = 2141701787
--select * from [pro-corporate].dbo.tblPAYE_Adjustment_Types 
--select GSC, GrossSalary + EER + HPD - HPC as GSCx, fee, HPD-HPC as HP, expenses, contractorid, paymentdate, sales, gsc, grosssalary, eer from #TMP_CoSelection

--delete any existing payments:  
DELETE FROM [pro-corporate].dbo.tblP11_u
--select * from [pro-corporate].dbo.tblP11_u
WHERE	tblP11_u.PUBLISHED is null and 
		exists (
				select	* 
				from	#TMP_CoSelection 
				where	#TMP_CoSelection.contractorID = tblP11_u.contractorID 
						and #TMP_CoSelection.paymentDate = [pro-corporate].dbo.tblP11_u.paymentDate
				)
--select [pro-corporate].dbo.fnContractorName(contractorID, 5) as c, * from #TMP_CoSelection where contractorID = 2141703194
--delete from #TMP_CoSelection where contractorID = 2141703194
		
----------------

INSERT INTO [pro-corporate].dbo.tblP11_u ( CompanyID, ContractorID, PaymentDate, PaymentMethod, ClearedDate, PeriodBasis, PAYENo, NoPeriods, 
			TaxYear, TaxCodeNo, TaxCodeLetter, PAYEbasis, NI_Letter,
			FirstEmployDate, LastEmployDate, ExpStatus, Additions, Deductions, EEE, EER, GrossSalary, GrossSalary_EER,
			PayToDate, TaxToDate, SLD_toDate, EEE_ToDate, 
			Pension_EER, Pension_PAE, Pension_Resv, 
			CCV, HP_Resv, PAYE, HPD, HPC, HP, HP_ToDate, GSC, 

			Expenses, 

			Exps_unused, ReadyToUse,

			P45PayToDate, P45TaxToDate, nmw, Sales, Advance, Fee, CreditLimit, AvFunds,
			PayTag, [notes], [user], [timestamp], CalcType,
			csa, sld
			--[A14], [A15], [A16], [A17]   --P11 fields (NEED TO APPEND INTO tblP11_u FIRST)
		      )

SELECT	gs.CompanyID,
--[pro-corporate].dbo.fnContractorName(gs.contractorID, 2) as c,
		gs.ContractorID,
		gs.PaymentDate,
		PayMethodDefault,
		ClearedDate, 
		gs.PeriodBasis, 
		gs.PAYENo, 
		gs.NoPeriods, 
		gs.TaxYear, 
		gs.TaxCodeNo, 
		gs.TaxCodeLetter, 
		gs. PAYEbasis, 
		gs.NI_Letter, 
		FirstEmployDate, 
		LastEmployDate,
		ExpStatus,
		Additions,

		--coalesce(PAYE_Deductions.Deductions,0) AS Deductions,
		0 AS Deductions,

		EEE, EER,

		gs.GrossSalary, GrossSalary_EER,
		PayToDate,
		TaxToDate,
		SLD_ToDate,

		EEptd AS EEE_ToDate,
		Pension_EER,
		Pension_PAE,
		Pension_Resv,
		CCV,
		HP_Resv,

		gs.PAYE,

		HPD,
		HPC,
		HP,
		HP_ToDate,

		GSC, 
		Expenses, 
		--ExpensesVAT, 
		exp_unused - Expenses AS Exps_unused,
		ReadyToUse, 

		gs.P45PayToDate, 
		gs.P45TaxToDate, 
		nmw, 
		Sales, 
		Advance,
		Fee,
		CreditLimit,
		AvFunds,
		0 AS PayTag, 

		case	when nmw+nmw_prev > gsc+gsc_prev then 'ERROR with National Minimum Wage - hours worked not covered.'

				else

				CASE 	WHEN GrossSalary_Prev >0 THEN 'Second Payment in tax week: Prev | EER £' + convert(varchar(10), EER_Prev) + ' | GS £' + convert(varchar(10), GrossSalary_Prev) + ' | PAYE £' + convert(varchar(10), PAYE_Prev) + ' | EEE £' + convert(varchar(10), EEE_Prev) 
						ELSE '' 
				END 

		end AS Notes,

		'proc_PayCalcs_u' AS [user], 
		getdate() AS [timestamp],
		PayType as CalcType,

		--PAYE deductions here:
		csa,
		sld

		--P11 fields:
		--coalesce(p.[1a], 0) as [A14],
		--coalesce(p.[1b], 0) as [A15],
		--coalesce(p.[1c], 0) as [A16],
		--coalesce(p.[1d], 0) as [A17]

FROM	#TMP_CoSelection gs
--left JOIN vw_P11_Fields p on	p.contractorID = gs.contractorID and 
--								p.paymentDate = gs.paymentDate and 
--								p.companyID = gs.companyID 

--where paytodate is null
WHERE 	not exists (select * FROM [pro-corporate].dbo.tblP11_u where gs.contractorID = tblP11_u.contractorID and tblP11_u.paymentDate = gs.paymentDate  )



--select * from #TMP1
--select paymentdate from #TMP_CoSelection
--select top 1 * from [pro-corporate].dbo.tblP11_u  order by timestamp desc
--delete from  [pro-corporate].dbo.tblP11_u  where p11_id = 794146


/*

update #TMP_CoSelection set NoPeriods = 1

select	[a14_prev],[a15_prev],[a16_prev],[a17_prev], grossSalary, NoPeriods,
		dbo.UDF_P11_1a(grossSalary, taxyear, PeriodBasis, NoPeriods) as [1a],
		dbo.UDF_P11_1b(grossSalary, taxyear, PeriodBasis, NoPeriods) as [1b],
		dbo.UDF_P11_1c(grossSalary, taxyear, PeriodBasis, NoPeriods) as [1c],
		dbo.UDF_P11_1d(grossSalary, taxyear, PeriodBasis, NoPeriods) as [1d]
from	#TMP_CoSelection

*/



UPDATE	[pro-corporate].dbo.tblP11_u 
SET		[A14] = coalesce([1a], 0) - coalesce(A14_prev, 0),
		[A15] = coalesce([1b], 0) - coalesce(A15_prev, 0), 
		[A16] = coalesce([1c], 0) - coalesce(A16_prev, 0),
		[A17] = coalesce([1d], 0) - coalesce(A17_prev, 0),
		[lastedittime] = getdate(),
		[lastedituser] = 'proc_PayCalcs_u'
--select #TMP_CoSelection.contractorID, [A14], [A15] ,[A16], [A17], [A14_prev], [A15_prev] ,[A16_prev], [A17_prev], coalesce([1a], 0), coalesce([1b], 0), coalesce([1c], 0), coalesce([1d], 0) 
FROM	[pro-corporate].dbo.tblP11_u 
INNER JOIN #TMP_CoSelection on 	#TMP_CoSelection.contractorID = [pro-corporate].dbo.tblP11_u.contractorID and 
								#TMP_CoSelection.paymentDate = [pro-corporate].dbo.tblP11_u.paymentDate and 
								#TMP_CoSelection.companyID = [pro-corporate].dbo.tblP11_u.companyID 
inner JOIN vw_P11_Fields_Grouped on	vw_P11_Fields_Grouped.PayeNo = [pro-corporate].dbo.tblP11_u.PayeNo and
									vw_P11_Fields_Grouped.taxyear = [pro-corporate].dbo.tblP11_u.taxyear and
									vw_P11_Fields_Grouped.contractorID = [pro-corporate].dbo.tblP11_u.contractorID and 
									vw_P11_Fields_Grouped.companyID = [pro-corporate].dbo.tblP11_u.companyID 
--LEFT JOIN vw_P11_Fields on vw_P11_Fields.P11_ID = [pro-corporate].dbo.tblP11_u.P11_ID 
WHERE	PUBLISHED IS NULL


--select [a14], [A14], [A15] ,[A16], [A17] from [pro-corporate].dbo.tblP11_u where paymentdate = '25 apr 2013' and contractorid = 2141702250

--Zero NoPeriods check process:  (puts list of ALL free periods in tblPAYEfreeWeeks)
--exec proc_PAYEfreeWeeksPopulate

--Second Payment in tax week: Prev | EER £0.00 | GS £76.00 | PAYE £0.00 | EEE £0.00

--select * from #TMP_CoSelection
--select contractorid  FROM [pro-corporate].dbo.tblP11_u where paymentdate = '7 jul 2010'


--Get AvFunds into ##avFunds   --
--declare @paymentdate datetime set @paymentdate = '27 feb 2015' 
exec proc_PayCalcs_u_AvailableFunds null, 1, 0, NULL, null, @PaymentDate
--select * from ##avFunds where contractorid = 2141700817


--declare @paymentdate datetime set @paymentdate = '27 feb 2015' 
UPDATE 	[pro-corporate].dbo.tblP11_u
SET 	[pro-corporate].dbo.tblP11_u.AvFunds = ##avFunds.AvFunds,
		[pro-corporate].dbo.tblP11_u.TakeHomeCalc = dbo.UDF_TakeHomeCalc(tblP11_u.p11_id)
--declare @paymentdate datetime set @paymentdate = '3 jan 2012' select tblP11_u.AvFunds, ##avFunds.AvFunds, tblP11_u.PaymentDate, dbo.UDF_TakeHomeCalc(tblP11_u.p11_id)
FROM	[pro-corporate].dbo.tblP11_u  
INNER JOIN ##avFunds ON ##avFunds.ContractorID = [pro-corporate].dbo.tblP11_u.ContractorID
WHERE	[pro-corporate].dbo.tblP11_u.PaymentDate = @PaymentDate and
		[pro-corporate].dbo.tblP11_u.Published is null


--select avFunds, TakeHomeCalc from [pro-corporate].dbo.tblP11_u where paymentdate = '17 aug 2010'

--dump expenses for today's payments in temp table:
delete	tblTMP_Expenses_Report
--select *
from	tblTMP_Expenses_Report
inner join #TMP_CoSelection on	#TMP_CoSelection.contractorID = tblTMP_Expenses_Report.contractorID and
								#TMP_CoSelection.PaymentDate = tblTMP_Expenses_Report.PaymentDate



insert into tblTMP_Expenses_Report (P11_ID, contractorID, PaymentDate, TrustStatus, Total, PaidOUT, NotPaidOUT, Pending, disallowed, NoExps_ReceiptRequired, ReadyToUse, [timestamp])
select	p11.P11_ID,
		er.contractorID,
		#TMP_CoSelection.PaymentDate,
		TrustStatus,
		round(Total, 2) as Total,
		PaidOUT,
		NotPaidOut,
		Pending,
		disallowed,
		NoExps_ReceiptRequired,
		#TMP_CoSelection.readyToUse,
		getdate() as [timestamp]
--select *
from	#vw_PayCalcs_Expenses er
inner join [pro-corporate].dbo.tblP11_u p11 on p11.contractorid = er.contractorid
inner join #TMP_CoSelection on	#TMP_CoSelection.ContractorID = p11.contractorid and
								#TMP_CoSelection.PaymentDate = p11.PaymentDate 

where	er.contractorID not in (select contractorID from tblTMP_Expenses_Report where PaymentDate = #TMP_CoSelection.PaymentDate)




--sets remitPayCalcsInvoices (sales to payment relationships):    declare @contractID int set @contractID = null
exec [pro-corporate_calcs].dbo.[proc_PayCalcs_u_POST] @contractID


--If Tuesday, run Statutory payments (eg. SSP, SMP):  select datepart(dw, getdate())
--Only run if a Tuesday:
--if  datepart(dw, getdate()) = 3
--BEGIN

	if  datepart(dw, getdate()) = 3		exec [pro-corporate_calcs].dbo.[proc_PayCalcs_u_Statutory]
	--select 'hello'

--END
---select timestamp, published from [pro-corporate].dbo.tblP11_u where paymentdate = [pro-corporate].dbo.dateonly(getdate()) order by timestamp desc



--select calcTypeTXT from vw_Payadvice where paymentdate = [pro-corporate].dbo.dateonly(getdate()) order by timestamp desc




























































































